<?php
    require_once('animal.php');
    require_once('ape.php');
    require_once('frog.php');
    
    $sheep = new Animal("shaun");

    echo "Name : ".$sheep->name."<br>"; // "shaun"
    echo "leg : ".$sheep->legs."<br>"; // 4
    echo "cold blooded : ".$sheep->cold_blooded."<br><br>"; // "no"
    
    // NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())

    $kodok = new Frog("buduk");
    echo "Name : ".$kodok->name."<br>"; // "shaun"
    echo "leg : ".$kodok->legs."<br>"; // 4
    echo "cold blooded : ".$kodok->cold_blooded."<br>"; // "no"
    echo "Jump : ";
    $kodok->jump();
    echo "<br><br>"; // "hop hop"

    $sungokong = new Ape("kera sakti");
    echo "Name : ".$sungokong->name."<br>"; // "shaun"
    echo "leg : ".$sungokong->legs."<br>"; // 4
    echo "cold blooded : ".$sungokong->cold_blooded."<br>"; // "no"
    echo "Yell : ";
    $sungokong->yell();
    echo "<br>" ;// "Auooo"
    ?>
